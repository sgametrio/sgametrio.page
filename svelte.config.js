import sveltePreprocess from "svelte-preprocess"
import node from "@sveltejs/adapter-node"
import netlify from "@sveltejs/adapter-netlify"
import static_ad from "@sveltejs/adapter-static"

let adapter = node
if (process.env.DEPLOYMENT_PLATFORM === "netlify") {
   adapter = netlify
}
if (process.env.DEPLOYMENT_PLATFORM === "static") {
   adapter = static_ad
}

/** @type {import('@sveltejs/kit').Config} */
const config = {
   preprocess: [
      sveltePreprocess({
         defaults: {
            style: "postcss",
         },
         postcss: true,
      }),
   ],
   kit: {
      // By default, `npm run build` will create a standard Node app.
      // You can create optimized builds for different platforms by
      // specifying a different adapter
      adapter: adapter(),

      // hydrate the <div id="svelte"> element in src/app.html
      target: "#svelte"
   },
}

export default config
