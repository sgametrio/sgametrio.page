## sgametrio.page

Personal website for @sgametrio.

### How to add a blog article?

1. Write a `.md` file and put it into the `blog-articles` folder.
2. Create a `.env.js` file based on `.env.example.js`

## PurgeCSS current behaviour

We include Tailwind but we are not able to fully purge its CSS.

Currently, the workflow is the following:

1. Create `tailwind.css` using Tailwind CLI and `tailwind.config.js`
2. PostCSS styles inside Svelte in order to purge unused classes of `tailwind.css`
3. Export the static website with Sapper

The functionalities that are not supported by purging, and therefore not used:

-  passing additional classes to child components
-  every functionality limited by the usage of Tailwind CLI
-  Dynamic classes in Svelte components seems not supported

**Workaround**: whitelist classes in `tailwind.config.js`
