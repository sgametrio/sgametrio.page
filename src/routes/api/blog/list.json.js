import { getArticles } from "$lib/helpers/articles"

export async function get({ params }) {
   const articles = getArticles()
   if (articles) {
      return {
         status: 200,
         headers: {
            "Content-type": "application/json",
         },
         body: articles,
      }
   }
}
