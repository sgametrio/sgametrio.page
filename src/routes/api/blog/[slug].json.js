import { getArticle } from "$lib/helpers/articles"

export async function get({ params }) {
   const article = getArticle(params.slug)
   if (article) {
      return {
         status: 200,
         headers: {
            "Content-type": "application/json",
         },
         body: article,
      }
   }
}
